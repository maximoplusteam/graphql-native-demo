import React, { Component } from "react";
import { View, Text } from "react-native";
import { Input, Button } from "react-native-elements";

class Details extends Component {
  constructor(props) {
    super(props);
    this.state = props.navigation.state.params.data;
  }
  render() {
    return (
      <View>
        <Input lebel="PO" value={this.state.ponum} editable={false} />
        <Input label="Status" value={this.state.status} editable={false} />
        <Input
          label="Buyer"
          value={this.state.purchaseagent}
          editable={false}
        />
        <Input label="Vendor" value={this.state.vendor} editable={false} />
        <Input label="Receipts" value={this.state.receipts} editable={false} />
        <Input
          label="Total Cost"
          value={this.state.totalcost && String(this.state.totalcost)}
          editable={false}
        />
        <Button
          title="Workflow"
          onPress={() => this.props.navigation.navigate("Workflow")}
        />
      </View>
    );
  }
}

export default Details;
