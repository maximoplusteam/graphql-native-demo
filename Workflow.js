import React, { Component } from "react";
import { View, Text } from "react-native";
import { Mutation } from "react-apollo";
import { Button } from "react-native-elements";
import gql from "graphql-tag";

const routeWFMutation = gql`
  mutation($handle: String, $processName: String) {
    routeWF(_handle: $handle, processName: $processName) {
      result {
        __typename
        ... on INPUTWF {
          actionid
          instruction
          list_actionid(fromRow: 0, numRows: 100) {
            actionid
            instruction
          }
        }
        ... on COMPLETEWF {
          actionid
          taskdescription
          instruction
          list_actionid(fromRow: 0, numRows: 100) {
            actionid
            instruction
          }
          memos(fromRow: 0, numRows: 100) {
            personid
            memo
            transdate
          }
        }

        ... on WFFINISHED {
          code
        }

        ... on INTERACTION {
          nextapp
          nexttab
        }
      }
      title
      responsetext
    }
  }
`;

const chooseWFMutation = gql`
  mutation($handle: String, $actionid: Int, $memo: String) {
    chooseWFAction(_handle: $handle, actionid: $actionid, memo: $memo) {
      result {
        __typename
        ... on INPUTWF {
          actionid
          instruction
          list_actionid(fromRow: 0, numRows: 100) {
            actionid
            instruction
          }
        }
        ... on COMPLETEWF {
          actionid
          taskdescription
          instruction
          list_actionid(fromRow: 0, numRows: 100) {
            actionid
            instruction
          }
          memos(fromRow: 0, numRows: 100) {
            personid
            memo
            transdate
          }
        }

        ... on WFFINISHED {
          code
        }

        ... on INTERACTION {
          nextapp
          nexttab
        }
      }
      title
      responsetext
    }
  }
`;

export default class Workflow extends Component {
  constructor(props) {
    super(props);
    this.state = { routed: false }; //before first route

    this.mutation = routeWFMutation; //start with the routewf mutation, all subsequent calls call the chooseWF mutation
  }
  render() {
    return (
      <Mutation mutation={this.state.mutation}>
        {(mutationF, { data, loading }) => {
          if (!this.state.routed) {
            return (
              <Button
                onPress={_ => {
                  mutationF({
                    variables: {
                      processName: this.props.data.processName,
                      handle: this.props.data.handle
                    }
                  });
                  this.setState({ routed: true, mutation: chooseWFMutation });
                }}
              />
            );
          } else {
            if (data) {
              return <View />;
            } else {
              if (loading) {
                return <Text>Loading...</Text>;
              } else {
                return <View />;
              }
            }
          }
        }}
      </Mutation>
    );
  }
}
