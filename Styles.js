import React, { Component } from "react";
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  app: {
    marginHorizontal: "auto",
    maxWidth: 500
  },
  logo: {
    height: 80
  },
  header: {
    padding: 20
  },
  title: {
    fontWeight: "bold",
    fontSize: 19,
    marginVertical: 3,
    textAlign: "center"
  },
  text: {
    lineHeight: 50,
    fontSize: 20,
    marginVertical: 5,
    textAlign: "center"
  },
  link: {
    color: "#1B95E0"
  },
  code: {
    fontFamily: "monospace, monospace"
  }
});
