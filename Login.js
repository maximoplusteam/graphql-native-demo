import React, { Component } from "react";
import { View, Button, TextInput } from "react-native";
import { encode } from "base-64";
//import AsyncStorage from "@react-native-community/async-storage";
//import { AsyncStorage } from "react-native-web";
import { AsyncStorage } from "react-native";

class Login extends Component {
  static navigationOptions = {
    title: "Please sign in"
  };
  constructor(props) {
    super(props);
    this.state = { username: "", password: "" };
  }

  signInAsync = async () => {
    const authHeader = encode(this.state.username + ":" + this.state.password);
    try {
      await AsyncStorage.setItem("token", authHeader);
      this.props.navigation.navigate("App");
    } catch (e) {
      console.warn(e);
    }
  };

  render() {
    return (
      <View>
        <TextInput
          value={this.state.username}
          onChangeText={text => this.setState({ username: text })}
        />
        <TextInput
          secureTextEntry={true}
          value={this.state.password}
          onChangeText={text => this.setState({ password: text })}
        />
        <Button title="Sign In" onPress={this.signInAsync} />
      </View>
    );
  }
}

export default Login;
