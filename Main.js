import React, { Component } from "react";
import {
  createSwitchNavigator,
  createStackNavigator,
  createAppContainer
} from "react-navigation";
import LoginScreen from "./Login.js";
import ListScreen from "./List.js";
import DetailsScreen from "./Details.js";
import NavigationService from "./NavigationService.js";
import WorkflowScreen from "./Workflow.js";

const AppStack = createStackNavigator({
  List: ListScreen,
  Details: DetailsScreen,
  Workflow: WorkflowScreen
});

const AuthStack = createStackNavigator({ Login: LoginScreen });

const AppContainer = createAppContainer(
  createSwitchNavigator(
    { App: AppStack, Auth: AuthStack },
    { initialRouteName: "App" }
  )
);

export default class App extends Component {
  render() {
    return (
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    );
  }
}
