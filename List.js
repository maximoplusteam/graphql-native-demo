import React, { Component } from "react";
import { View, Text, FlatList } from "react-native";
import gql from "graphql-tag";
import { Query } from "react-apollo";
import styles from "./Styles.js";
import { ListItem } from "react-native-elements";

const poQuery = gql`
  query($qbe: POQBE) {
    po(fromRow: 0, numRows: 20, qbe: $qbe) {
      ponum
      description
      status
      purchaseagent
      vendor
      receipts
      totalcost
      id
      _handle
      
    }
  }
`;

const qbe = { status: "=WAPPR" };

const POList = props => (
  <Query query={poQuery} variables={{ qbe }}>
    {({ loading, error, data }) => {
      if (loading) return <Text styles={styles.text}>Loading...</Text>;
      if (error) return <Text styles={styles.text}>Error :(</Text>;
      return (
        <FlatList
          data={data.po}
          keyExtractor={item => item.id}
          renderItem={({ item }) => (
            <ListItem
              title={item.ponum}
              subtitle={item.description}
              onPress={() =>
                props.navigation.navigate("Details", { data: item })
              }
            />
          )}
        />
      );
    }}
  </Query>
);

export default POList;
