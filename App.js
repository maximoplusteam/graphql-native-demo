import React from "react";
import { ApolloProvider } from "react-apollo";
import Main from "./Main";
import client from "./GraphQLLink";

export default function App() {
  return (
    <ApolloProvider client={client}>
      <Main />
    </ApolloProvider>
  );
}
