import NavigationService from "./NavigationService.js";
import { ApolloClient } from "apollo-client";
import { ApolloProvider } from "react-apollo";
import { HttpLink, createHttpLink } from "apollo-link-http";
import { ApolloLink } from "apollo-link";
import { onError } from "apollo-link-error";
import { InMemoryCache } from "apollo-cache-inmemory";
import { setContext } from "apollo-link-context";

//import AsyncStorage from "@react-native-community/async-storage";
import { AsyncStorage } from "react-native";

const httpLink = createHttpLink({
  uri: "http://192.168.1.107:4001/graphql",
  credentials: "include"
});

const authLink = setContext(async (_, { headers }) => {
  // get the authentication token from local storage if it exists
  //  const token = localStorage.getItem("token");
  const token = await AsyncStorage.getItem("token");
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Basic ${token}` : ""
    }
  };
});

const errorHandlerLink = onError(({ response, networkError }) => {
  if (networkError) {
    if (networkError.statusCode === 401) {
      AsyncStorage.removeItem("token");
      NavigationService.navigate("Login");
    } else {
      NavigationService.navigate("Error", { error: networkError });
      //display the error (for example network error)
    }
  }
});

export default new ApolloClient({
  link: errorHandlerLink.concat(authLink).concat(httpLink),
  cache: new InMemoryCache()
});
