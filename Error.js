import React, { Component } from "react";
import { View, Text } from "react-native";
import Styles from "./Styles";

export default props => (
  <View>
    <Text style={Styles.title}>ERROR:</Text>
    <Text style={Styles.text}>{props.navigation.errorText}</Text>
  </View>
);
